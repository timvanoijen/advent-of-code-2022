package dag3;

import utils.Utils;

import java.util.Iterator;
import java.util.stream.Collectors;

public class Puzzle2 {

    private static final int dag = 3;

    public static void main(String[] args) {

        String alfabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        int total = 0;
        Iterator<String> lines = Utils.getInputLines(dag, false).toList().iterator();
        while(lines.hasNext()) {
            String s1 = lines.next();
            String s2 = lines.next();
            String s3 = lines.next();
            String c = intersection(intersection(s1, s2), s3);
            int priorityBest = alfabet.indexOf(c) + 1;
            total += priorityBest;
        }

        System.out.println(total);
    }

    private static String intersection(String a, String b) {
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < a.length(); i++) {
            String ai = a.substring(i, i + 1);
            if (b.contains(ai) && !result.toString().contains(ai)) {
                result.append(ai);
            }
        }
        return result.toString();
    }
}
