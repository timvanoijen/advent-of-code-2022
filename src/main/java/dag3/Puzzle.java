package dag3;

import utils.Utils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 3;

    public static void main(String[] args) {

        String alfabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        int total = 0;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            Set<String> o = new HashSet<>();
            int priorityBest = 0;
            int i = -1;
            while(priorityBest == 0 && ++i < s.length()) {
                String si = s.substring(i, i+1);
                if (i >= s.length() / 2) {
                    if (o.contains(si)) {
                        priorityBest = alfabet.indexOf(si) + 1;
                    }
                } else {
                    o.add(si);
                }
            }
            total += priorityBest;
        }

        System.out.println(total);
    }
}
