package dag18;

import utils.Utils;

import java.util.*;

public class Puzzle2 {

    private static final int dag = 18;

    record Cube(int x, int y, int z) {
        public Cube add(Cube other) {
            return new Cube(x + other.x, y + other.y, z + other.z);
        }

        public Cube max(Cube other) {
            return new Cube(Math.max(x, other.x), Math.max(y, other.y), Math.max(z, other.z));
        }
    }

    private static final List<Cube> neighbourDeltas = Arrays.asList(
            new Cube(1, 0, 0),
            new Cube(-1, 0, 0),
            new Cube(0, 1, 0),
            new Cube(0, -1, 0),
            new Cube(0, 0, 1),
            new Cube(0, 0, -1)
    );

    public static void main(String[] args) {

        Map<Cube, Boolean> cubeIsExterior = new HashMap<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            String[] coords = s.split(",");
            cubeIsExterior.put(new Cube(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2])),
                    false);
        }

        Cube maxDimensions = cubeIsExterior.keySet().stream().reduce(new Cube(0,0,0), Cube::max);

        int totalExteriorSides = 0;
        for(Cube cube : cubeIsExterior.keySet().stream().toList()) {
            for(Cube neighbourDelta : neighbourDeltas) {
                Cube sideCube = cube.add(neighbourDelta);
                if (isExterior(sideCube, cubeIsExterior, maxDimensions))
                    totalExteriorSides++;
            }
        }
        System.out.println(totalExteriorSides);
    }

    private static boolean isExterior(Cube cube, Map<Cube, Boolean> cubeIsExterior, Cube maxDimensions) {
        Set<Cube> visited = new HashSet<>();
        Deque<Cube> toAnalyse = new ArrayDeque<>();
        toAnalyse.add(cube);
        boolean isExterior = false;
        while(toAnalyse.peekFirst() != null) {
            Cube cur = toAnalyse.poll();
            if (!visited.add(cur) || !cubeIsExterior.getOrDefault(cur, true))
                continue;
            if (cur.x < 0 || cur.x > maxDimensions.x ||
                cur.y < 0 || cur.y > maxDimensions.y ||
                cur.z < 0 || cur.z > maxDimensions.z ||
                cubeIsExterior.getOrDefault(cur, false)) {
                isExterior = true;
                break;
            }
            toAnalyse.addAll(neighbourDeltas.stream().map(cur::add).toList());
        }
        for(Cube c : visited)
            cubeIsExterior.putIfAbsent(c, isExterior);
        return isExterior;
    }
}
