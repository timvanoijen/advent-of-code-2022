package dag18;

import utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Puzzle {

    private static final int dag = 18;

    public static void main(String[] args) {

        record Cube(int x, int y, int z) {}

        List<Cube> cubes = new ArrayList<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            String[] coords = s.split(",");
            cubes.add(new Cube(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2])));
        };

        long sidesCovered = cubes.stream().mapToLong(c1 -> cubes.stream()
                .filter(c2 -> Math.abs(c1.x - c2.x) + Math.abs(c1.y - c2.y) + Math.abs(c1.z - c2.z) == 1)
                .count()
        ).sum();

        long totalUncovered = cubes.size() * 6 - sidesCovered;

        System.out.println(totalUncovered);
    }
}
