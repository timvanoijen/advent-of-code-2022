package dag15;

import utils.Utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Puzzle {

    private static final int dag = 15;

    record Interval(int x1, int x2) { };
    record Point(int x, int y) { };

    public static void main(String[] args) {

        Pattern p = Pattern.compile("Sensor at x=(.*), y=(.*): closest beacon is at x=(.*), y=(.*)");

        Map<Integer, Set<Interval>> vertIntervals = new HashMap<>();
        Map<Integer, Set<Interval>> horIntervals = new HashMap<>();
        Set<Point> points = new HashSet<>();

        for(String s : Utils.getInputLines(dag, false).toList()) {
            Matcher m = p.matcher(s);
            m.find();
            int sx = Integer.parseInt(m.group(1));
            int sy = Integer.parseInt(m.group(2));
            int bx = Integer.parseInt(m.group(3));
            int by = Integer.parseInt(m.group(4));

            //points.add(new Point(sx, sy));
            points.add(new Point(bx, by));

            int dist = Math.abs(bx - sx) + Math.abs(by - sy);
            for(int delta = -dist; delta <= dist; delta++) {
                int intervalW = dist - Math.abs(delta);
                Interval horInterval = new Interval(sx - intervalW, sx + intervalW);
                Interval vertInterval = new Interval(sy - intervalW, sy + intervalW);

                Set<Interval> horIntervalsForY = horIntervals.computeIfAbsent(sy + delta, k -> new HashSet<>());
                Set<Interval> vertIntervalsForX = vertIntervals.computeIfAbsent(sx + delta, k -> new HashSet<>());
                addInterval(horIntervalsForY, horInterval);
                addInterval(vertIntervalsForX, vertInterval);
            }
        }

        int y = 2000000;
        int total = horIntervals.get(y).stream().mapToInt(i -> i.x2 - i.x1 + 1).sum();
        total -= points.stream().filter(p2 -> p2.y == y).count();
        System.out.println(total);
    }

    private static void addInterval(Set<Interval> intervals, Interval newInterval) {
        for(Interval existingInterval : intervals.stream().toList()) {
            boolean overlap = (existingInterval.x2 >= newInterval.x1 && newInterval.x2 >= existingInterval.x1);
            if (!overlap)
                continue;
            intervals.remove(existingInterval);
            newInterval = new Interval(Math.min(existingInterval.x1, newInterval.x1),
                    Math.max(existingInterval.x2, newInterval.x2));
        }
        intervals.add(newInterval);
    }
}
