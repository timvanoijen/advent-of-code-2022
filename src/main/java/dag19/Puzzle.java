package dag19;

import utils.Utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class Puzzle {

    private static final int dag = 19;

    private record State(int[] resources, int[] robots, int roundsRemaining) {

        @Override
        public boolean equals(Object o) {
            State s = (State)o;
            return Arrays.equals(resources, s.resources) &&
                Arrays.equals(robots, s.robots) &&
                roundsRemaining == s.roundsRemaining;
        }

        @Override
        public int hashCode() {
            return Objects.hash(Arrays.hashCode(resources), Arrays.hashCode(robots), roundsRemaining);
        }
    }

    private static Map<State, Integer> cache = new HashMap<>();

    public static void main(String[] args) {

        Pattern ptrn = Pattern.compile("Blueprint \\d+: Each ore robot costs (\\d+) ore. Each clay robot costs (\\d+) ore. " +
                "Each obsidian robot costs (\\d+) ore and (\\d+) clay. Each geode robot costs (\\d+) ore and (\\d+) obsidian.");

        List<Integer> totalBests = new ArrayList<>();
        int id = 0;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            id++;
            cache = new HashMap<>();

            // One blueprint
            Matcher m = ptrn.matcher(s);
            m.find();

            // Parse
            int[][] needs = new int[4][4];
            needs[0][0] = Integer.parseInt(m.group(1));
            needs[1][0] = Integer.parseInt(m.group(2));
            needs[2][0] = Integer.parseInt(m.group(3));
            needs[2][1] = Integer.parseInt(m.group(4));
            needs[3][0] = Integer.parseInt(m.group(5));
            needs[3][2] = Integer.parseInt(m.group(6));

            // Get geodes
            int[] startResources = new int[4];
            int[] startRobots = new int[] { 1, 0, 0, 0};
            int[] maxResourceNeeds = new int[4];
            for(int i = 0; i < 4; i++)
                for(int j = 0; j < 4; j++)
                    maxResourceNeeds[j] = Math.max(maxResourceNeeds[j], needs[i][j]);
            maxResourceNeeds[3] = Integer.MAX_VALUE;

            totalBests.add(id * memoized(needs, maxResourceNeeds, new State(startResources, startRobots, 24)));
            System.out.println(totalBests.get(totalBests.size() - 1));
        }
        System.out.println(totalBests.stream().reduce(0, Integer::sum));
    }

    private static int memoized(int[][] needs, int[] maxResourceNeeds, State state) {
        if (cache.containsKey(state)) {
            return cache.get(state);
        }
        int score = getGeodes(needs, maxResourceNeeds, state);
        cache.put(state, score);
        return score;
    }

    private static int getGeodes(int[][] needs, int[] maxResourceNeeds, State state) {
        if (state.roundsRemaining == 0)
            return state.resources[3];

        List<Integer> scores = new ArrayList<>();
        int[] resourcesAfterProduction = IntStream.range(0, state.resources.length)
                .map(i -> robotNotNeeded(maxResourceNeeds, state, i) ? Integer.MAX_VALUE : (state.resources[i] + state.robots[i]))
                .toArray();

        scores.add(memoized(needs, maxResourceNeeds,
                new State(resourcesAfterProduction, state.robots, state.roundsRemaining - 1)));

        for(int i = 0; i < 4; i++) {

            if (robotNotNeeded(maxResourceNeeds, state, i))
                continue;

            int[] newRobots = state.robots.clone();
            newRobots[i] = newRobots[i] + 1;

            int[] newResources = resourcesAfterProduction.clone();
            boolean canBuildRobot = true;
            for(int j = 0; j < state.resources.length; j++) {
                if (state.resources[j] < needs[i][j]) {
                    canBuildRobot = false;
                    break;
                }
                if (newResources[j] != Integer.MAX_VALUE)
                    newResources[j] = newResources[j] - needs[i][j];
            }
            if (canBuildRobot) {
                State newState = new State(newResources, newRobots, state.roundsRemaining - 1);
                scores.add(memoized(needs, maxResourceNeeds, newState));
            }
        }
        return scores.stream().mapToInt(i -> i).max().getAsInt();
    }

    private static boolean robotNotNeeded(int[] maxResourceNeeds, State state, int i) {
        return maxResourceNeeds[i] != Integer.MAX_VALUE &&
                (state.resources[i] == Integer.MAX_VALUE ||
                        ((state.roundsRemaining - 1) * maxResourceNeeds[i]) <= state.resources[i] + ((state.roundsRemaining - 2) * state.robots[i]));
    }
}
