package dag8;

import utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Puzzle2 {

    private static final int dag = 8;

    public static void main(String[] args) {

        record Point(int x, int y) { }

        List<List<Integer>> m = new ArrayList<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            m.add(new ArrayList<>());
            for(String s2 : s.split("")) {
                m.get(m.size() - 1).add(Integer.parseInt(s2));
            }
        }

        int width = m.get(0).size();
        int height = m.size();

        Stream<Stream<Point>> st = Stream.empty();
        st = Stream.concat(st, IntStream.range(0, height).mapToObj(y -> IntStream.range(0, width).mapToObj(x -> new Point(x, y))));
        st = Stream.concat(st, IntStream.range(0, height).mapToObj(y -> IntStream.range(0, width).mapToObj(x -> new Point(width - 1 - x, y))));
        st = Stream.concat(st, IntStream.range(0, width).mapToObj(x -> IntStream.range(0, height).mapToObj(y -> new Point(x, y))));
        st = Stream.concat(st, IntStream.range(0, width).mapToObj(x -> IntStream.range(0, height).mapToObj(y -> new Point(x, height - 1 - y))));

        long[][] scores = new long[height][width];
        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++)
                scores[y][x] = 1;

        for(Stream<Point> sp : st.toList()) {
            int[] scenicdistance = new int[10];
            for(Point p : sp.toList()) {
                int h = m.get(p.y).get(p.x);
                scores[p.y][p.x] = scores[p.y][p.x] * scenicdistance[h];
                for(int h2 = 0; h2 < 10; h2++) {
                    scenicdistance[h2] = h2 <= h ? 1 : (scenicdistance[h2] + 1);
                }
            }
        }

        long total = Arrays.stream(scores).flatMapToLong(Arrays::stream).max().orElseThrow();
        System.out.println(total);
    }
}
