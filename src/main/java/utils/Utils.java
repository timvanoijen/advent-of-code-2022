package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Utils {
    public static Stream<String> getInputLines(int dag, boolean test) {
        String fileName = String.format("/code/advent-of-code-2022/src/main/java/dag%s/input%s.txt",
                dag, test ? "_test" : "");
        try {
            return Files.lines(Paths.get(fileName));
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    public static Stream<String> getInputLines(int dag) {
        return getInputLines(dag, false);
    }
}
