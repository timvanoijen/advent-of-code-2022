package dag25;

import utils.Utils;

public class Puzzle {

    private static final int dag = 25;

    public static void main(String[] args) {

        long total = 0;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            total += snafuToLong(s);
        };
        System.out.println(longToSnafu(total));
    }

    private static long snafuToLong(String s) {
        long res = 0;
        for(String c : s.split("")) {
            res = res * 5 + switch(c) {
                case "2" -> 2;
                case "1" -> 1;
                case "-" -> -1;
                case "=" -> -2;
                default -> 0;
            };
        }
        return res;
    }

    private static String longToSnafu(long n) {
        long rem = n;
        long ePrev = 0;
        String res = "";
        while(rem != 0) {
            long e = (long)Math.ceil(Math.log(Math.abs(rem)) / Math.log(5));
            long d = Math.round((double)rem / Math.pow(5, e));
            if (d == 0) {
                e--;
                d = Math.round((double)rem / Math.pow(5, e));
            }
            rem -= d * Math.pow(5, e);

            for(long i = ePrev; i > e + 1; i--)
                res += "0";
            ePrev = e;

            res += switch((int)d) {
                case -2 -> "=";
                case -1 -> "-";
                case 1 -> "1";
                case 2 -> "2";
                default -> 0;
            };
        }

        while (--ePrev >= 0)
            res += "0";

        return res;
    }
}
