package dag24;

import utils.Utils;

import java.util.*;

public class Puzzle2 {

    private static final int dag = 24;

    private record Point(int x, int y) {}

    private record Blizzard(int id, Point dir) {}

    private record State(Point curPos, int rnd, long minCost) {}

    private static final List<Set<Point>> occupiedForRnd = new ArrayList<>();
    private static final List<Map<Blizzard, Point>> blizzardPosForRnd = new ArrayList<>();

    private static final List<Point> directions = List.of(
            new Point(0, 0),
            new Point(1, 0),
            new Point(0, 1),
            new Point(-1, 0),
            new Point(0, -1)
    );

    private static int width;
    private static int height;

    public static void main(String[] args) {

        List<String> lines = Utils.getInputLines(dag, false).toList();
        height = lines.size() - 2;
        width = lines.get(0).length() - 2;
        blizzardPosForRnd.add(new HashMap<>());
        occupiedForRnd.add(new HashSet<>());

        // Parse
        int id = 0;
        for(int y = 1; y < lines.size() - 1; y++) {
            List<String> cs = Arrays.stream(lines.get(y).split("")).toList();
            for(int x = 1; x < cs.size() - 1; x++) {
                String c = cs.get(x);
                if (".".equals(c))
                    continue;
                Point dir = switch(c) {
                    case "^" -> new Point(0, -1);
                    case ">" -> new Point(1, 0);
                    case "v" -> new Point(0, 1);
                    default -> new Point(-1, 0);
                };
                Point pos = new Point(x - 1, y - 1);
                blizzardPosForRnd.get(0).put(new Blizzard(++id, dir), pos);
                occupiedForRnd.get(0).add(pos);
            }
        }

        // Find best
        Point start = new Point(0, -1);
        Point end = new Point(width - 1, height);
        int n1 = nrRounds(start, end, 0);
        int n2 = nrRounds(end, start, n1 - 1);
        int n3 = nrRounds(start, end, n2 - 1);
        System.out.println(n3 - 1);
    }

    private static int nrRounds(Point start, Point end, int rnd0) {
        PriorityQueue<State> q = new PriorityQueue<>(Comparator.comparingLong(State::minCost));
        Set<State> analysed = new HashSet<>();
        State s0 = new State(start, rnd0, minCost(start, 0));
        q.add(s0);

        while(!q.isEmpty()) {
            State s = q.poll();
            if (s.curPos.equals(end)) {
                return s.rnd + 1;
            }

            if (analysed.contains(s))
                continue;
            Point p = s.curPos;
            for(Point dir : directions) {
                Point pNew = new Point(p.x + dir.x, p.y + dir.y);
                if (!isFree(pNew, s.rnd + 1))
                    continue;
                State sNew = new State(pNew, s.rnd + 1, minCost(pNew, s.rnd + 1));
                if (analysed.contains(sNew))
                    continue;
                q.add(sNew);
            }
            analysed.add(s);
        }
        throw new AssertionError();
    }

    private static long minCost(Point pos, int rnd) {
        return rnd + (width - 1 - pos.x) + (height - 1 - pos.y);
    }

    private static boolean isFree(Point pos, int rnd) {
        if ((pos.x < 0 || pos.x >= width || pos.y < 0 || pos.y >= height)
                && !pos.equals(new Point(0, -1))
                && !pos.equals(new Point(width - 1, height)))
            return false;

        if (rnd == occupiedForRnd.size()) {
            blizzardPosForRnd.add(new HashMap<>());
            occupiedForRnd.add(new HashSet<>());

            for(Map.Entry<Blizzard, Point> kvp : blizzardPosForRnd.get(rnd - 1).entrySet()) {
                Point p = new Point(kvp.getValue().x + kvp.getKey().dir.x, kvp.getValue().y + kvp.getKey().dir.y);
                p = new Point((p.x + width) % width, (p.y + height) % height);
                blizzardPosForRnd.get(rnd).put(kvp.getKey(), p);
                occupiedForRnd.get(rnd).add(p);
            }
        }
        return !occupiedForRnd.get(rnd).contains(pos);
    }
}
