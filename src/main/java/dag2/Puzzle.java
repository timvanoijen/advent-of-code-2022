package dag2;

import utils.Utils;

import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 2;

    public static void main(String[] args) {

        int total = 0;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            String[] parts = s.split(" ");
            int p1 = parts[0].equals("A") ? 0 : ( parts[0].equals("B") ? 1 : 2);
            int p2 = parts[1].equals("X") ? 0 : ( parts[1].equals("Y") ? 1 : 2);

            int score = p2 + 1 + ((((3 + p2 - p1) % 3) == 1) ? 6 : (p2 == p1 ? 3 : 0));
            total += score;
        }

        System.out.println(total);
    }
}
