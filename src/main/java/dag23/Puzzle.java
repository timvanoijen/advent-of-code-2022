package dag23;

import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 23;

    private record Point(int x, int y) {
        Point add(Point o) {
            return new Point(x + o.x, y + o.y);
        }
    }

    private static final List<List<Point>> directions = List.of(
            List.of(new Point(0, -1), new Point(-1, -1), new Point(1, -1)),
            List.of(new Point(0, 1), new Point(-1, 1), new Point(1, 1)),
            List.of(new Point(-1, 0), new Point(-1, -1), new Point(-1, 1)),
            List.of(new Point(1, 0), new Point(1, -1), new Point(1, 1))
    );

    public static void main(String[] args) {

        // Parse
        int y = 0;
        Set<Point> elves = new HashSet<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            int x = 0;
            for(String c : s.split("")) {
                if (c.equals("#"))
                    elves.add(new Point(x, y));
                x++;
            }
            y++;
        }

        // Iterate over rounds
        for(int rnd = 0; rnd < 10; rnd++) {
            rnd++;
            Map<Point, Point> moves = new HashMap<>();
            for(Point elf : elves) {

                // No elves around, stay.
                if (directions.stream().allMatch(dps -> dps.stream().noneMatch(p -> elves.contains(elf.add(p)))))
                    continue;

                // Otherwise, propose direction
                for(int dir = 0; dir < 4; dir++) {
                    List<Point> dps = directions.get((dir + rnd) % 4);
                    if (dps.stream().noneMatch(p -> elves.contains(elf.add(p)))) {
                        Point to = elf.add(dps.get(0));
                        if (moves.containsKey(to))
                            moves.remove(to);
                        else
                            moves.put(to, elf);
                        break;
                    }
                }
            }

            for(Map.Entry<Point, Point> move : moves.entrySet()) {
                elves.remove(move.getValue());
                elves.add(move.getKey());
            }
        }

        // Answer
        int minX = elves.stream().mapToInt(Point::x).min().getAsInt();
        int maxX = elves.stream().mapToInt(Point::x).max().getAsInt();
        int minY = elves.stream().mapToInt(Point::y).min().getAsInt();
        int maxY = elves.stream().mapToInt(Point::y).max().getAsInt();
        System.out.println(((maxX - minX + 1) * (maxY - minY + 1)) - elves.size());
    }
}
