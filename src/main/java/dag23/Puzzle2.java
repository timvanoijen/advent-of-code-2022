package dag23;

import utils.Utils;

import java.util.*;

public class Puzzle2 {

    private static final int dag = 23;

    private record Point(int x, int y) {
        Point add(Point o) {
            return new Point(x + o.x, y + o.y);
        }
    }

    private static final List<List<Point>> directions = List.of(
            List.of(new Point(0, -1), new Point(-1, -1), new Point(1, -1)),
            List.of(new Point(0, 1), new Point(-1, 1), new Point(1, 1)),
            List.of(new Point(-1, 0), new Point(-1, -1), new Point(-1, 1)),
            List.of(new Point(1, 0), new Point(1, -1), new Point(1, 1))
    );

    public static void main(String[] args) {

        // Parse
        int y = 0;
        Set<Point> elves = new HashSet<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            int x = 0;
            for(String c : s.split("")) {
                if (c.equals("#"))
                    elves.add(new Point(x, y));
                x++;
            }
            y++;
        }

        // Iterate over rounds
        int rnd = 0;
        while(true) {
            rnd++;
            Map<Point, Point> moves = new HashMap<>();
            boolean stuck = true;
            for(Point elf : elves) {

                // No elves around, stay.
                if (directions.stream().allMatch(dps -> dps.stream().noneMatch(p -> elves.contains(elf.add(p)))))
                    continue;
                stuck = false;

                // Otherwise, propose direction
                for(int dir = 0; dir < 4; dir++) {
                    List<Point> dps = directions.get((dir + rnd + 3) % 4);
                    if (dps.stream().noneMatch(p -> elves.contains(elf.add(p)))) {
                        Point to = elf.add(dps.get(0));
                        if (moves.containsKey(to))
                            moves.remove(to);
                        else
                            moves.put(to, elf);
                        break;
                    }
                }
            }

            if (stuck) {
                System.out.println(rnd);
                break;
            }

            /*if (moves.entrySet().size() == 0) {
                stuckCnt++;
                if (stuckCnt == 4) {
                    System.out.println(rnd - 3);
                    break;
                }

            } else
                stuckCnt = 0;*/


            for(Map.Entry<Point, Point> move : moves.entrySet()) {
                elves.remove(move.getValue());
                elves.add(move.getKey());
            }
        }
    }
}
