package dag22;

import org.apache.commons.lang3.tuple.Pair;
import utils.Utils;

import java.util.*;

public class Puzzle2 {

    private static final int dag = 22;

    private record Point(int x, int y) {}

    private static final Set<Point> blocked = new HashSet<>();

    private static final Map<Pair<Point, Integer>, Pair<Point, Integer>> teletransports = new HashMap<>();
    private static int sideWidth;

    private static final List<Point> directions = List.of(
            new Point(1, 0),
            new Point(0, 1),
            new Point(-1, 0),
            new Point( 0, -1)
    );

    public static void main(String[] args) {

        boolean isTest = false;

        // Set teletransports
        sideWidth = isTest ? 4 : 50;
        if (isTest) {
            processTeletransports(2, 0, 3, 0, 1, 1, true); // 1 top -> 2 top
            processTeletransports(2, 0, 0, 3, 2, 2, true); // 1 right -> 6 right
            processTeletransports(2, 0, 2, 1, 1, 1, false); // 1 left -> 3 top
            processTeletransports(0, 1, 3, 2, 0, 1, true); // 2 top -> 1 top
            processTeletransports(0, 1, 2, 3, 2, 3, true); // 2 left -> 6 down
            processTeletransports(0, 1, 1, 2, 2, 3, true); // 2 down -> 5 down
            processTeletransports(1, 1, 3, 2, 0, 0, false); // 3 top -> 1 left
            processTeletransports(1, 1, 1, 2, 2, 0, true); // 3 down -> 5 left
            processTeletransports(2, 2, 2, 1, 1, 3, true); // 5 left -> 3 down
            processTeletransports(2, 2, 1, 0, 1, 3, true); // 5 down -> 2 down
            processTeletransports(3, 2, 1, 0, 1, 0, true); // 6 down -> 2 left
            processTeletransports(3, 2, 0, 2, 0, 2, true); // 6 right -> 1 right
            processTeletransports(3, 2, 3, 2, 1, 2, true); // 6 top -> 4 right
            processTeletransports(2, 1, 0, 3, 2, 1, true); // 4 right -> 6 top
        } else {
            processTeletransports(1, 0, 3, 0, 3, 0, false); // 1 top -> 6 left
            processTeletransports(2, 0, 3, 0, 3, 3, false); // 2 top -> 6 down
            processTeletransports(2, 0, 0, 1, 2, 2, true); // 2 right -> 4 right
            processTeletransports(2, 0, 1, 1, 1, 2, false); // 2 down -> 3 right
            processTeletransports(1, 1, 0, 2, 0, 3, false); // 3 right -> 2 down
            processTeletransports(1, 2, 0, 2, 0, 2, true); // 4 right -> 2 right
            processTeletransports(1, 2, 1, 0, 3, 2, false); // 4 down -> 6 right
            processTeletransports(0, 3, 0, 1, 2, 3, false); // 6 right -> 4 down
            processTeletransports(0, 3, 1, 2, 0, 1, false); // 6 down -> 2 top
            processTeletransports(0, 3, 2, 1, 0, 1, false); // 6 left -> 1 top
            processTeletransports(0, 2, 2, 1, 0, 0, true); // 5 left -> 1 left
            processTeletransports(0, 2, 3, 1, 1, 0, false); // 5 top -> 3 left
            processTeletransports(1, 1, 2, 0, 2, 1, false); // 3 left -> 5 top
            processTeletransports(1, 0, 2, 0, 2, 0, true); // 1 left -> 5 left
        }

        // Parse
        int y = 0;
        List<String> input = Utils.getInputLines(dag, isTest).toList();
        for (String s : input) {
            if (s.isEmpty())
                break;

            int x = 0;
            for (String pp : s.split("")) {
                if (pp.equals("#"))
                    blocked.add(new Point(x, y));
                x++;
            }
            y++;
        }

        String code = input.get(input.size() - 1);

        // Walk
        Point curPoint = isTest ? new Point(2 * sideWidth, 0) : new Point(sideWidth, 0);
        int dir = 0;

        int codePos = 0;
        boolean expectNumber = true;

        while (codePos < code.length()) {
            if (expectNumber) {
                int codePosInitial = codePos;
                while (codePos < code.length() && code.charAt(codePos) != 'L' && code.charAt(codePos) != 'R')
                    codePos++;
                int nr = Integer.parseInt(code.substring(codePosInitial, codePos));
                while(nr-- > 0) {
                    Point newPoint = new Point(curPoint.x + directions.get(dir).x, curPoint.y + directions.get(dir).y);
                    int newDir = dir;
                    Pair<Point, Integer> teletransport = teletransports.get(Pair.of(newPoint, dir));
                    if (teletransport != null) {
                        newPoint = teletransport.getLeft();
                        newDir = teletransport.getRight();
                    }
                    if (blocked.contains(newPoint))
                        break;
                    curPoint = newPoint;
                    dir = newDir;
                }
            } else {
                dir = (dir + 4 + (code.charAt(codePos++) == 'R' ? 1 : -1)) % 4;
            }
            expectNumber = !expectNumber;
        }

        System.out.println(1000 * (curPoint.y + 1) + 4 * (curPoint.x + 1) + dir);
    }

    private static void processTeletransports(int fromX, int fromY, int fromDir, int toX, int toY, int toDir,
                                              boolean switchOrder) {
        for(int i = 0; i < sideWidth; i++) {
            Point ptFrom = switch(fromDir) {
                case 0 -> new Point((fromX + 1) * sideWidth, fromY * sideWidth + i);
                case 1 -> new Point(fromX * sideWidth + i, (fromY + 1) * sideWidth);
                case 2 -> new Point(fromX * sideWidth - 1, fromY * sideWidth + i);
                default -> new Point(fromX * sideWidth + i, fromY * sideWidth - 1);
            };
            int j = switchOrder ? sideWidth - 1 - i : i;
            Point ptTo = switch(toDir) {
                case 2 -> new Point((toX + 1) * sideWidth - 1, toY * sideWidth + j);
                case 3 -> new Point(toX * sideWidth + j, (toY + 1) * sideWidth - 1);
                case 0 -> new Point(toX * sideWidth, toY * sideWidth + j);
                default -> new Point(toX * sideWidth + j, toY * sideWidth);
            };
            teletransports.put(Pair.of(ptFrom, fromDir), Pair.of(ptTo, toDir));
        }
    }
}
