package dag22;

import utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Puzzle {

    private static final int dag = 22;

    private record Point(int x, int y, boolean free) {}

    private static final List<List<Point>> rows = new ArrayList<>();
    private static final List<List<Point>> columns = new ArrayList<>();
    private static final Map<Point, Integer> pointToRowCol = new HashMap<>();
    private static final Map<Point, Integer> pointToColRow = new HashMap<>();

    private static final List<Point> directions = List.of(
            new Point(1, 0, true),
            new Point(0, 1, true),
            new Point(-1, 0, true),
            new Point( 0, -1, true)
    );

    public static void main(String[] args) {

        // Parse
        int y = 0;
        List<String> input = Utils.getInputLines(dag, false).toList();
        for (String s : input) {
            if (s.isEmpty())
                break;

            if (y > rows.size() - 1)
                rows.add(new ArrayList<>());

            int x = -1;
            for (String pp : s.split("")) {
                x++;
                if (x > columns.size() - 1)
                    columns.add(new ArrayList<>());
                if (pp.equals(" "))
                    continue;
                Point p = new Point(x, y, pp.equals("."));
                rows.get(y).add(p);
                columns.get(x).add(p);
                pointToRowCol.put(p, rows.get(y).size() - 1);
                pointToColRow.put(p, columns.get(x).size() - 1);
            }
            y++;
        }

        String code = input.get(input.size() - 1);

        // Walk
        Point curPoint = rows.get(0).get(0);
        int dir = 0;

        int codePos = 0;
        boolean expectNumber = true;

        while (codePos < code.length()) {
            if (expectNumber) {
                int codePosInitial = codePos;
                while (codePos < code.length() && code.charAt(codePos) != 'L' && code.charAt(codePos) != 'R')
                    codePos++;
                int nr = Integer.parseInt(code.substring(codePosInitial, codePos));
                while(nr-- > 0) {
                    Point newPoint;
                    if (directions.get(dir).x != 0) {
                        int curCol = pointToRowCol.get(curPoint);
                        List<Point> row = rows.get(curPoint.y);
                        newPoint = row.get((curCol + row.size() + directions.get(dir).x) % row.size());
                    } else {
                        int curRow = pointToColRow.get(curPoint);
                        List<Point> col = columns.get(curPoint.x);
                        newPoint = col.get((curRow + col.size() + directions.get(dir).y) % col.size());
                    }
                    if (!newPoint.free)
                        break;
                    curPoint = newPoint;
                }
            } else {
                dir = (dir + 4 + (code.charAt(codePos++) == 'R' ? 1 : -1)) % 4;
            }
            expectNumber = !expectNumber;
        }

        System.out.println(1000 * (curPoint.y + 1) + 4 * (curPoint.x + 1) + dir);
    }
}
