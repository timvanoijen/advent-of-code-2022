package dag11;

import utils.Utils;

import java.util.*;
import java.util.function.LongBinaryOperator;
import java.util.function.LongUnaryOperator;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 11;

    public static void main(String[] args) {

        record Monkey(LongUnaryOperator operation, long divisibleBy, int trueMonkey, int falseMonkey) {}

        List<Monkey> monkeys = new ArrayList<>();
        List<List<Long>> items = new ArrayList<>();
        Iterator<String> it = Utils.getInputLines(dag, false).toList().iterator();
        while(it.hasNext()) {
            it.next();
            String startingItemsString = it.next().substring(18);
            List<Long> startingItems = Arrays.stream(startingItemsString.split(", "))
                    .map(Long::parseLong).collect(Collectors.toList());
            String operationString = it.next();
            LongBinaryOperator operator = operationString.charAt(23) == '+' ? Long::sum : (a, b) -> a * b;
            String operationArg = operationString.substring(25);
            LongUnaryOperator operationArgSupplier = operationArg.equals("old") ? i -> i : i -> Long.parseLong(operationArg);
            LongUnaryOperator operation = i -> operator.applyAsLong(i, operationArgSupplier.applyAsLong(i));

            long divisibleBy = Long.parseLong(it.next().substring(21));
            int trueMonkey = Integer.parseInt(it.next().substring(29));
            int falseMonkey = Integer.parseInt(it.next().substring(30));

            Monkey monkey = new Monkey(operation, divisibleBy, trueMonkey, falseMonkey);
            monkeys.add(monkey);
            items.add(startingItems);
            if (it.hasNext())
                it.next();
        }

        long divProd = monkeys.stream().map(Monkey::divisibleBy).reduce(1L, (a, b) -> a * b);
        List<Long> totals = new ArrayList<>(Collections.nCopies(monkeys.size(), 0L));
        for(int r = 0; r < 10000; r++) {
            for(int mi = 0; mi < monkeys.size(); mi++) {
                Monkey m = monkeys.get(mi);
                for(long w : items.get(mi)) {
                    totals.set(mi, totals.get(mi) + 1);
                    w = m.operation().applyAsLong(w); // / 3;
                    int miTo = (w % m.divisibleBy == 0L) ? m.trueMonkey : m.falseMonkey;
                    items.get(miTo).add(w % divProd);
                }
                items.get(mi).clear();
            }
        }
        totals.sort(Collections.reverseOrder());
        System.out.println(totals.get(0) * totals.get(1));
    }
}
