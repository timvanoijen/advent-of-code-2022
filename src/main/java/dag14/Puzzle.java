package dag14;

import utils.Utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Puzzle {

    private static final int dag = 14;

    public static void main(String[] args) {

        record Point(int x, int y) {};

        // Read rocks
        Set<Point> rocks = new HashSet<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            String[] p = s.split(" -> ");
            List<Point> pL = Arrays.stream(p).map(p2 -> p2.split(","))
                    .map(p2 -> new Point(Integer.parseInt(p2[0]), Integer.parseInt(p2[1])))
                    .toList();
            for(int i = 0; i < pL.size() - 1; i++) {
                int dx = pL.get(i + 1).x - pL.get(i).x;
                int dy = pL.get(i + 1).y - pL.get(i).y;
                Point dp = dx != 0 ? new Point(dx / Math.abs(dx), 0) : new Point(0, dy / Math.abs(dy));

                Point cur = pL.get(i);
                rocks.add(cur);
                while (!cur.equals(pL.get(i + 1))) {
                    cur = new Point(cur.x + dp.x, cur.y + dp.y);
                    rocks.add(cur);
                }
            }
        };

        // Simulate water falling
        int total = 0;
        int maxY = rocks.stream().mapToInt(Point::y).max().getAsInt();
        List<Point> moves = Arrays.asList(new Point(0, 1), new Point(-1, 1), new Point(1, 1));

        boolean isSpilling = false;
        while(!isSpilling) {
            total++;
            Point p = new Point(500, 0);
            boolean isRest = false;
            while(!isRest && !isSpilling) {
                isRest = true;
                for(Point move : moves) {
                    Point pNew = new Point(p.x + move.x, p.y + move.y);
                    if (!rocks.contains(pNew)) {
                        p = pNew;
                        isSpilling = pNew.y == maxY;
                        isRest = false;
                        break;
                    }
                }
            }
            rocks.add(p);
        }

        System.out.println(total - 1);
    }
}
