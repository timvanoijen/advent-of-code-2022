package dag20;

import utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Puzzle2 {

    private static final int dag = 20;

    private record NumberWrapper(long n, int idx) {}

    public static void main(String[] args) {

        List<String> input = Utils.getInputLines(dag, false).toList();

        List<NumberWrapper> lOriginal = new ArrayList<>();
        int cnt = 0;
        for(String s : input) {
            long n = Integer.parseInt(s);
            lOriginal.add(new NumberWrapper(n * 811589153, cnt++));
        }
        List<NumberWrapper> l = new ArrayList<>(lOriginal);

        for(int rnd = 0; rnd < 10; rnd++) {
            for(NumberWrapper p : lOriginal.stream().toList()) {
                int idx = l.indexOf(p);
                l.remove(idx);
                int newIdx = (int) (((long) idx + p.n) % l.size());
                if (newIdx < 0)
                    newIdx += l.size();
                l.add(newIdx , p);
            }
        }

        int idx0 = l.indexOf(l.stream().filter(nw -> nw.n == 0).findFirst().orElseThrow());
        System.out.println(l.get((idx0 + 1000) % l.size()).n +
                l.get((idx0 + 2000) % l.size()).n +
                l.get((idx0 + 3000) % l.size()).n);
    }
}
