package dag7;

import utils.Utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 7;

    public static void main(String[] args) {

        // Read file structure
        Pattern filePattern = Pattern.compile("(\\d+) (.*)");
        TreeNode root = new TreeNode(null, 0);
        TreeNode curDir = root;
        Deque<String> de = Utils.getInputLines(dag, false).collect(Collectors.toCollection(ArrayDeque::new));
        while(!de.isEmpty()) {
            String s = de.pollFirst();
            if (s.equals("$ cd /")) {
                curDir = root;
            } else if (s.equals("$ cd ..")) {
                curDir = curDir.parent;
            } else if (s.startsWith("$ cd ")) {
                curDir = curDir.getOrAddDir(s.substring(5));
            } else {
                assert s.equals("$ ls");
                while(de.peekFirst() != null && !de.peekFirst().startsWith("$")) {
                    String o = de.pollFirst();
                    Matcher m = filePattern.matcher(o);
                    if (m.matches()) {
                        curDir.addFile(m.group(2), Long.parseLong(m.group(1)));
                    }
                }
            }
        }

        // Get answer 1
        long total = root.getDirs().stream().mapToLong(TreeNode::getSize).filter(sz -> sz <= 100000).sum();
        System.out.println(total);

        // Get answer 2
        long totalUsedSpace = root.getSize();
        long freeSpace = 70000000 - totalUsedSpace;
        long spaceToFree = 30000000 - freeSpace;

        long actuallyFreedSpace = root.getDirs().stream()
                .mapToLong(TreeNode::getSize)
                .filter(s -> s >= spaceToFree)
                .sorted()
                .findFirst().orElseThrow();
        System.out.println(actuallyFreedSpace);
    }

    private static class TreeNode {

        private final HashMap<String, TreeNode> children = new HashMap<>();
        private final TreeNode parent;
        private final long size;

        public TreeNode(TreeNode parent, long size) {
            this.parent = parent;
            this.size = size;
        }

        public TreeNode getOrAddDir(String name) {
            return children.computeIfAbsent(name, n -> new TreeNode(this, 0));
        }

        public void addFile(String name, long size) {
            TreeNode fileNode = new TreeNode(this, size);
            children.put(name, fileNode);
        }

        public List<TreeNode> getDirs() {
            List<TreeNode> result = new ArrayList<>();
            if (size == 0) {
                result.add(this);
            }
            for(TreeNode child : children.values()) {
                result.addAll(child.getDirs());
            }
            return result;
        }

        public long getSize() {
            return size + children.values().stream().mapToLong(TreeNode::getSize).sum();
        }
    }
}
