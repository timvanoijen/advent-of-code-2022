package dag9;

import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Puzzle {

    private static final int dag = 9;

    public static void main(String[] args) {
        System.out.println(calcNrSeenPositions(2));
        System.out.println(calcNrSeenPositions(10));
    }

    private static int calcNrSeenPositions(int nrKnots) {
        record Point(int x, int y) {}

        List<Point> hl = Stream.generate(() -> new Point(0, 0)).limit(nrKnots).toList();
        Set<Point> seen = new HashSet<>();
        seen.add(hl.get(nrKnots - 1));
        for(String s : Utils.getInputLines(dag, false).toList()) {
            String dir = s.substring(0, 1);
            int nrMoves = Integer.parseInt(s.substring(2));
            Point dh = switch(dir) {
                case "U" -> new Point(0, 1);
                case "R" -> new Point(1, 0);
                case "D" -> new Point(0, -1);
                default -> new Point (-1, 0);
            };

            while(nrMoves-- > 0) {
                hl.set(0, new Point(hl.get(0).x + dh.x, hl.get(0).y + dh.y));
                for (int i = 1; i < nrKnots; i++) {
                    Point h = hl.get(i - 1);
                    Point t = hl.get(i);
                    if (Math.max(Math.abs(h.x - t.x), Math.abs(h.y - t.y)) > 1) {
                        t = new Point(t.x + (h.x == t.x ? 0 : (h.x - t.x) / Math.abs(h.x - t.x)),
                                t.y + (h.y == t.y ? 0 : (h.y - t.y) / Math.abs(h.y - t.y)));
                    }
                    hl.set(i, t);
                }
                seen.add(hl.get(nrKnots - 1));
            }
        }

        return seen.size();
    }
}
