package dag12;

import utils.Utils;

import java.util.*;
import java.util.function.LongBinaryOperator;
import java.util.function.LongUnaryOperator;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 12;

    public static void main(String[] args) {

        record Point(int x, int y) { }
        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        List<String> lines = Utils.getInputLines(dag, false).toList();
        int height = lines.size();
        int width = lines.get(0).length();
        Map<Point, Integer> heights = new HashMap<>();
        Point start = null;
        Point end = null;
        for(int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Character c = lines.get(y).charAt(x);
                if (c.equals('S')) {
                    start = new Point(x, y);
                    c = 'a';
                } else if (c.equals('E')) {
                    end = new Point(x, y);
                    c = 'z';
                }
                heights.put(new Point(x, y), alphabet.indexOf(c));
            }
        }

        Map<Point, Integer> costs = new HashMap<>();
        Map<Point, List<Point>> paths = new HashMap<>();
        PriorityQueue<Point> q = new PriorityQueue<>(Comparator.comparingInt(p -> costs.get(p)));
        costs.put(start, 0);
        q.add(start);

        List<Point> dpL = Arrays.asList(new Point(-1, 0), new Point(1, 0), new Point(0, -1), new Point(0, 1));
        while(!q.isEmpty() && !costs.containsKey(end)) {
            Point p = q.poll();
            for(Point dp : dpL) {
                int xNew = p.x + dp.x;
                int yNew = p.y + dp.y;
                if (xNew < 0 || xNew > width - 1 || yNew < 0 || yNew > height - 1)
                    continue;
                Point pNew = new Point(xNew, yNew);
                if (heights.get(pNew) > heights.get(p) + 1)
                    continue;

                int costNew = costs.get(p) + 1;

                if (costs.getOrDefault(pNew, Integer.MAX_VALUE) <= costNew)
                    continue;

                List<Point> pathNew = new ArrayList<>(paths.computeIfAbsent(p, dmy -> Collections.emptyList()));
                pathNew.add(p);
                paths.put(pNew, pathNew);
                costs.put(pNew, costNew);
                q.add(pNew);
            }
        }

        System.out.println(costs.get(end));
    }
}
