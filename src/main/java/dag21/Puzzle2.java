package dag21;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import utils.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.LongBinaryOperator;
import java.util.function.LongUnaryOperator;
import java.util.stream.Collectors;

public class Puzzle2 {

    private static final int dag = 21;

    private record Monkey(String name, Expression expression) { }

    private enum Operator {

        PLUS("+", (a, b) -> a + b, (a, b) -> a - b, true),
        MINUS("-", (a, b) -> a - b, (a, b) -> a + b, false),
        TIMES("*", (a, b) -> a * b, (a, b) -> a / b, true),
        DIV("/", (a, b) -> a / b, (a, b) -> a * b, false);

        String s;
        LongBinaryOperator f;
        LongBinaryOperator inverse;
        boolean commutative;

        Operator(String s, LongBinaryOperator f, LongBinaryOperator inverse, boolean commutative) {
            this.s = s;
            this.f = f;
            this.inverse = inverse;
            this.commutative = commutative;
        }
    }

    private interface Expression {
        long evaluate(long humn);
    }

    private static class HumnExpression implements Expression {

        @Override
        public long evaluate(long humn) {
            return humn;
        }

        @Override
        public String toString() {
            return "humn";
        }
    }

    @AllArgsConstructor
    private static class ConstantExpression implements Expression {
        private final long constant;
        @Override
        public long evaluate(long humn) {
            return constant;
        }

        @Override
        public String toString() {
            return Long.toString(constant);
        }
    }

    @AllArgsConstructor
    private static class ComposedExpression implements Expression {
        private final Operator operator;
        private final Expression left;
        private final Expression right;

        @Override
        public long evaluate(long humn) {
            return operator.f.applyAsLong(left.evaluate(humn), right.evaluate(humn));
        }

        @Override
        public String toString() {
            return "(" + left.toString() + operator.s + right.toString() + ")";
        }

        public boolean isSimplifiable() {
            return left instanceof ConstantExpression || right instanceof ConstantExpression;
        }

        public Pair<Expression, LongUnaryOperator> simplify() {
            if (left instanceof ConstantExpression) {
                if (operator.commutative)
                    return Pair.of(right, n -> operator.inverse.applyAsLong(n, left.evaluate(0)));
                else
                    return Pair.of(right, n -> operator.f.applyAsLong(left.evaluate(0), n));
            } else if (right instanceof ConstantExpression) {
                return Pair.of(left, n -> operator.inverse.applyAsLong(n, right.evaluate(0)));
            }
            throw new AssertionError();
        }
    }

    private static Monkey getMonkey(String name, Map<String, Monkey> existing, Map<String, String> lines) {
        if (existing.containsKey(name))
            return existing.get(name);
        String s = lines.get(name);
        String rem = s.substring(6);
        Expression expression;
        if (name.equals("humn")) {
            expression = new HumnExpression();
        } else if (rem.length() < 4) {
            expression = new ConstantExpression(Integer.parseInt(rem));
        }
        else {
            Monkey a1 = getMonkey(rem.substring(0, 4), existing, lines);
            Monkey a2 = getMonkey(rem.substring(7), existing, lines);
            Operator o = switch (rem.charAt(5)) {
                case '+'-> Operator.PLUS;
                case '-'-> Operator.MINUS;
                case '*'-> Operator.TIMES;
                default -> Operator.DIV;
            };
            if (a1.expression instanceof ConstantExpression && a2.expression instanceof ConstantExpression) {
                expression = new ConstantExpression(o.f.applyAsLong(a1.expression.evaluate(0), a2.expression.evaluate(0)));
            } else {
                expression = new ComposedExpression(o, a1.expression, a2.expression);
            }
        }
        Monkey m = new Monkey(name, expression);
        existing.put(name, m);
        return m;
    }

    public static void main(String[] args) {

        Map<String, Monkey> monkeys = new HashMap<>();
        Map<String, String> lines = Utils.getInputLines(dag, false).collect(Collectors.toMap(
                l -> l.substring(0, 4), Function.identity()));

        Expression lhs = getMonkey(lines.get("root").substring(6, 10), monkeys, lines).expression;
        Expression rhs = getMonkey(lines.get("root").substring(13), monkeys, lines).expression;

        // Reduce
        while(true) {
            if (lhs instanceof ConstantExpression && rhs instanceof ComposedExpression) {
                if (((ComposedExpression) rhs).isSimplifiable()) {
                    Pair<Expression, LongUnaryOperator> p = ((ComposedExpression) rhs).simplify();
                    lhs = new ConstantExpression(p.getRight().applyAsLong(lhs.evaluate(0)));
                    rhs = p.getLeft();
                }
            } else if (rhs instanceof ConstantExpression && lhs instanceof ComposedExpression) {
                if (((ComposedExpression) lhs).isSimplifiable()) {
                    Pair<Expression, LongUnaryOperator> p = ((ComposedExpression) lhs).simplify();
                    rhs = new ConstantExpression(p.getRight().applyAsLong(rhs.evaluate(0)));
                    lhs = p.getLeft();
                }
            } else {
                break;
            }
        }

        long n = -1;
        while(true) {
            if (lhs.evaluate(++n) == rhs.evaluate(n)) {
                System.out.println(n);
                break;
            }
        }
    }
}
