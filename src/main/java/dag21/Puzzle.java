package dag21;

import utils.Utils;

import java.util.*;
import java.util.function.Function;
import java.util.function.LongBinaryOperator;

public class Puzzle {

    private static final int dag = 21;

    record Monkey(String name, Function<Map<String, Monkey>, Optional<Long>> numberSupplier, Optional<Long> number) { }

    public static void main(String[] args) {

        Map<String, Monkey> monkeys = new HashMap<>();

        // Parse
        for(String s : Utils.getInputLines(dag, false).toList()) {
            String name = s.substring(0, 4);
            String rem = s.substring(6);
            Function<Map<String, Monkey>, Optional<Long>> numberSupplier;
            if (rem.length() > 3) {
                String a1 = rem.substring(0, 4);
                String a2 = rem.substring(7);
                LongBinaryOperator o = switch (rem.charAt(5)) {
                    case '+'-> (a,b) -> a + b;
                    case '-'-> (a,b) -> a - b;
                    case '*'-> (a,b) -> a * b;
                    default -> (a,b) -> a / b;
                };
                numberSupplier = m -> {
                    Optional<Long> o1 = m.containsKey(a1) ? m.get(a1).number : Optional.empty();
                    Optional<Long> o2 = m.containsKey(a2) ? m.get(a2).number : Optional.empty();
                    if (o1.isEmpty() || o2.isEmpty())
                        return Optional.empty();
                    return Optional.of(o.applyAsLong(o1.get(), o2.get()));
                };
            } else
                numberSupplier = m -> Optional.of(Long.parseLong(rem));
            monkeys.put(name, new Monkey(name, numberSupplier, Optional.empty()));
        };

        // Evaluate
        int n = -1;
        while(true) {
            n++;
            while (true) {
                for (Monkey m : monkeys.values().stream().toList()) {
                    if (m.number.isPresent())
                        continue;
                    Optional<Long> o = m.numberSupplier.apply(monkeys);
                    if (o.isPresent()) {
                        monkeys.remove(m.name);
                        monkeys.put(m.name, new Monkey(m.name, m.numberSupplier, o));
                    }
                }
                if (monkeys.get("root").number.isPresent()) {
                    System.out.println(monkeys.get("root").number.get());
                    break;
                }
            }
        }
    }
}
