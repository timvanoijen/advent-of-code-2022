package dag13;

import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 13;

    record Tree(int value, Tree parent, List<Tree> children, boolean isNumeric) {
        @Override
        public String toString() {
            if (isNumeric) {
                return Integer.toString(value);
            }
            return "[" + children.stream().map(Tree::toString).collect(Collectors.joining(",")) + "]";
        }
    };

    public static void main(String[] args) {

        int total = 0;
        int round = 0;
        Iterator<String> it = Utils.getInputLines(dag, false).iterator();
        while(it.hasNext()) {
            round++;
            String s1 = it.next();
            String s2 = it.next();
            if (it.hasNext())
                it.next();

            Tree t1 = readTree(s1);
            Tree t2 = readTree(s2);

            if (!t2.toString().equals("[" + s2 + "]")) {
                System.out.println("Jo");
            }

            if (compare(t1, t2) <= 0) {
                total += round;
            }
        }

        System.out.println(total);
    }

    private static int compare(Tree t1, Tree t2) {
        if (t1.isNumeric && t2.isNumeric) {
            return t1.value  - t2.value;
        } else if (t1.isNumeric) {
            return compare(new Tree(0, null, Collections.singletonList(t1), false), t2);
        } else if (t2.isNumeric) {
            return compare(t1, new Tree(0, null, Collections.singletonList(t2), false));
        }
        for(int i = 0; i < Math.min(t1.children.size(), t2.children.size()); i++) {
            int r = compare(t1.children.get(i), t2.children.get(i));
            if (r != 0) {
                return r;
            }
        }
        return t1.children.size() - t2.children.size();
    }

    private static Tree readTree(String s) {
        Tree cur = new Tree(0, null, new ArrayList<>(), false);
        Iterator<String> it = Arrays.stream(s.split("")).iterator();
        while(it.hasNext()) {
            String c = it.next();
            if (c.equals("[")) {
                Tree child = new Tree(0, cur, new ArrayList<>(), false);
                cur.children.add(child);
                cur = child;
            } else if (c.equals("]")) {
                if (cur.isNumeric) {
                    cur = cur.parent;
                }
                cur = cur.parent;
            } else if (c.equals(",")) {
                if (cur.isNumeric) {
                    cur = cur.parent;
                }
                //Tree child = new Tree(0, cur.parent, new ArrayList<>());
                //cur.parent.children.add(child);
                //cur = child;
            } else { // numeric
                int d = Integer.parseInt(c);
                if (cur.isNumeric) {
                    cur.parent.children.set(cur.parent.children.size() - 1,
                            new Tree(cur.value * 10 + d, cur.parent, new ArrayList<>(), true));
                } else {
                    Tree child = new Tree(d, cur, new ArrayList<>(), true);
                    cur.children.add(child);
                    cur = child;
                }
            }
        }
        return cur;
    }
}
