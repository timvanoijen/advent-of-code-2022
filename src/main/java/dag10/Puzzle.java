package dag10;

import utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Puzzle {

    private static final int dag = 10;

    public static void main(String[] args) {

        List<Integer> states = new ArrayList<>();
        states.add(1);
        int pos = 1;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            states.add(pos);
            if (!s.equals("noop")) {
                pos = pos + Integer.parseInt(s.substring(5));
                states.add(pos);
            }
        }

        int c = 20;
        int total = 0;
        while(c < states.size()) {
            total += c * states.get(c - 1);
            c += 40;
        }

        System.out.println(total);
    }
}
