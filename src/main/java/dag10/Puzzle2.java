package dag10;

import utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Puzzle2 {

    private static final int dag = 10;

    public static void main(String[] args) {

        List<Integer> states = new ArrayList<>();
        states.add(1);
        int pos = 1;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            states.add(pos);
            if (!s.equals("noop")) {
                pos = pos + Integer.parseInt(s.substring(5));
                states.add(pos);
            }
        }

        for(int y = 0; y < 6; y++) {
            String r = "";
            for(int x = 0; x < 40; x++) {
                int xs = states.get(40*y + x);
                r += (Math.abs(x - xs) <= 1) ? "#" : ".";
            }
            System.out.println(r);
        }
    }
}
