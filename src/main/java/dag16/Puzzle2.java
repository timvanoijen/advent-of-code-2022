package dag16;

import utils.Utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Puzzle2 {

    private static final int dag = 16;

    private static Pattern ptrn = Pattern.compile("Valve (.*) has flow rate=(\\d+); tunnels? leads? to valves? (.*)");

    private static class Location {
        int idx;
        int flowrate;
        List<Location> neighbours = new ArrayList<>();
    }

    record State(List<Location> l, int round, long openedValves, int flowRateAcc, int flowRatePerRound) { }

    private static int nrRounds = 26;

    public static void main(String[] args) {

        Map<String, Location> locations = new HashMap<>();

        int locationIdx = 0;

        // Parse and build graph
        for(String s : Utils.getInputLines(dag, false).toList()) {
            Matcher m = ptrn.matcher(s);
            m.find();
            String name = m.group(1);
            int flowrate = Integer.parseInt(m.group(2));
            String neighbours = m.group(3);

            Location l = locations.computeIfAbsent(name, n -> new Location());
            l.idx = locationIdx++;
            l.flowrate = flowrate;
            for(String nbName : neighbours.split(", ")) {
                Location nb = locations.computeIfAbsent(nbName, n -> new Location());
                l.neighbours.add(nb);
            }
        }

        // Find optimal path
        Map<List<Location>, Map<Long, Map<Integer, Integer>>> bestsForRoundForValveStateForLocation = new HashMap<>();

        int upperBoundFlowRatePerRound = locations.values().stream().map(l -> l.flowrate)
                .sorted(Comparator.reverseOrder()).limit(nrRounds).mapToInt(i -> i).sum();
        Queue<State> q = new PriorityQueue<>(Comparator.comparingInt(a -> -upperBoundForState(a, upperBoundFlowRatePerRound)));
        State s = new State(Arrays.asList(locations.get("AA"), locations.get("AA")), 0, 0, 0, 0);
        q.add(s);
        int highestRound = 0;
        while(q.peek() != null) {
            s = q.poll();
            if (s.round == nrRounds)
                break;

            if (s.round > highestRound) {
                System.out.println(s.round);
                highestRound = s.round;
            }

            // Actions
            record StateDelta(Location l, long openedValvesDiff, int flowRateDiff) {}
            List<List<StateDelta>> potentialStateDeltasList = new ArrayList<>();
            for(Location l : s.l) {
                List<StateDelta> potentialStateDeltas = new ArrayList<>();
                if ((s.openedValves & (1 << l.idx)) == 0) {
                    if (l.flowrate > 0) {
                        StateDelta s2 = new StateDelta(l, (1 << l.idx), l.flowrate);
                        potentialStateDeltas.add(s2);
                    }
                }
                for (Location nb : l.neighbours) {
                    StateDelta s2 = new StateDelta(nb, 0, 0);
                    potentialStateDeltas.add(s2);
                }
                potentialStateDeltasList.add(potentialStateDeltas);
            }

            // Cartesian product of the two potential valves and position deltas.
            State sFinal = s;
            List<State> potentialStates = potentialStateDeltasList.get(0).stream().flatMap(sd1 ->
                    potentialStateDeltasList.get(1).stream()
                            .filter(sd2 -> (sd2.openedValvesDiff == 0 || sd2.openedValvesDiff != sd1.openedValvesDiff))
                            .map(sd2 -> new State(Stream.of(sd1.l, sd2.l).sorted(Comparator.comparingInt(a -> a.idx)).toList(),
                                    sFinal.round + 1,
                                    sFinal.openedValves + sd1.openedValvesDiff + sd2.openedValvesDiff,
                                    sFinal.flowRateAcc + sFinal.flowRatePerRound,
                                    sFinal.flowRatePerRound + sd1.flowRateDiff + sd2.flowRateDiff))
            ).toList();

            for(State s2 : potentialStates) {
                if (canBeBest(s2, bestsForRoundForValveStateForLocation))
                    q.add(s2);
            }
        }

        System.out.println(s.flowRateAcc);
    }

    private static boolean canBeBest(State s, Map<List<Location>, Map<Long, Map<Integer, Integer>>> bestsForRoundForValveStateForLocation) {
        Map<Integer,Integer> bestsForValveState = bestsForRoundForValveStateForLocation
                .computeIfAbsent(s.l, d -> new HashMap<>())
                .computeIfAbsent(s.openedValves, d -> new HashMap<>());
        int lb = lowerBoundForState(s);
        if (lb > bestsForValveState.getOrDefault(s.round, -1)) {
            bestsForValveState.put(s.round, lb);
            return true;
        }
        return false;
    }

    private static int lowerBoundForState(State s) {
        return s.flowRateAcc + (nrRounds - s.round) * s.flowRatePerRound;
    }

    private static int upperBoundForState(State s, int sumFlowRates) {
        return s.flowRateAcc + (nrRounds - s.round) * sumFlowRates;
    }
}
