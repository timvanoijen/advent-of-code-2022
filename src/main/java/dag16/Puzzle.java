package dag16;

import utils.Utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Puzzle {

    private static final int dag = 16;

    private static Pattern ptrn = Pattern.compile("Valve (.*) has flow rate=(\\d+); tunnels? leads? to valves? (.*)");

    private static class Location {
        int idx;
        int flowrate;
        List<Location> neighbours = new ArrayList<>();
    }

    record State(Location l, int round, long openedValves, int flowRateAcc, int flowRatePerRound) { }

    private static int nrRounds = 30;

    public static void main(String[] args) {

        Map<String, Location> locations = new HashMap<>();

        int locationIdx = 0;
        // Parse and build graph
        for(String s : Utils.getInputLines(dag, false).toList()) {
            Matcher m = ptrn.matcher(s);
            m.find();
            String name = m.group(1);
            int flowrate = Integer.parseInt(m.group(2));
            String neighbours = m.group(3);

            Location l = locations.computeIfAbsent(name, n -> new Location());
            l.idx = locationIdx++;
            l.flowrate = flowrate;
            for(String nbName : neighbours.split(", ")) {
                Location nb = locations.computeIfAbsent(nbName, n -> new Location());
                l.neighbours.add(nb);
            }
        }

        // Find optimal path
        Map<Location, Map<Long, Map<Integer, Integer>>> bestsForRoundForValveStateForLocation = new HashMap<>();

        int sumFlowRates = locations.values().stream().map(l -> l.flowrate)
                .sorted(Comparator.reverseOrder()).limit(nrRounds / 2).mapToInt(i -> i).sum();
        Queue<State> q = new PriorityQueue<>(Comparator.comparingInt(a -> -upperBoundForState(a, sumFlowRates)));
        State s = new State(locations.get("AA"), 0, 0, 0, 0);
        q.add(s);
        int highestRound = 0;
        while(q.peek() != null) {
            s = q.poll();
            if (s.round == nrRounds)
                break;

            if (s.round > highestRound) {
                System.out.println(s.round);
                highestRound = s.round;
            }

            // Actions
            List<State> potentialStates = new ArrayList<>();
            if ((s.openedValves & (1 << s.l.idx)) == 0) {
                if (s.l.flowrate > 0) {
                    State s2 = new State(s.l, s.round + 1,
                            s.openedValves + (1 << s.l.idx),
                            s.flowRateAcc + s.flowRatePerRound,
                            s.flowRatePerRound + s.l.flowrate);
                    potentialStates.add(s2);
                }
            }
            for(Location nb : s.l.neighbours) {
                State s2 = new State(nb, s.round + 1,
                        s.openedValves,s.flowRateAcc + s.flowRatePerRound, s.flowRatePerRound);
                potentialStates.add(s2);
            }

            for(State s2 : potentialStates) {
                if (canBeBest(s2, bestsForRoundForValveStateForLocation.computeIfAbsent(s2.l, d -> new HashMap<>()))) {
                    bestsForRoundForValveStateForLocation
                            .computeIfAbsent(s2.l, d -> new HashMap<>())
                            .computeIfAbsent(s2.openedValves, d -> new HashMap<>())
                            .put(s2.round, lowerBoundForState(s2));
                    q.add(s2);
                }
            }
        }

        System.out.println(s.flowRateAcc);
    }

    private static boolean canBeBest(State s, Map<Long, Map<Integer, Integer>> bestsForRoundForValveState) {
        Map<Integer, Integer> bestsPerRound = bestsForRoundForValveState
                .computeIfAbsent(s.openedValves, d -> new HashMap<>());
        for(int rnd = s.round; rnd == s.round; rnd--) {
            if (bestsPerRound.getOrDefault(rnd, -1) >= lowerBoundForState(s))
                return false;
        }
        return true;
    }

    private static int lowerBoundForState(State s) {
        return s.flowRateAcc + (nrRounds - s.round) * s.flowRatePerRound;
    }

    private static int upperBoundForState(State s, int sumFlowRates) {
        return s.flowRateAcc + (nrRounds - s.round) * sumFlowRates;
    }
}
