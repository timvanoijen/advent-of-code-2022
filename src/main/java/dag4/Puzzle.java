package dag4;

import utils.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 4;

    public static void main(String[] args) {

        Pattern p = Pattern.compile("(\\d+)-(\\d+),(\\d+)-(\\d+)");

        int total = 0;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            Matcher m = p.matcher(s);
            m.find();
            long i_start = Long.parseLong(m.group(1));
            long i_end = Long.parseLong(m.group(2));
            long j_start = Long.parseLong(m.group(3));
            long j_end = Long.parseLong(m.group(4));

            if ((i_start >= j_start && i_end <= j_end) || (j_start >= i_start && j_end <= i_end)) {
                total++;
            }
        }

        System.out.println(total);
    }
}
