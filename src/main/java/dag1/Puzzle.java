package dag1;

import utils.Utils;

import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 1;

    public static void main(String[] args) {

        int total = 0;
        int totalBest = 0;
        for(String s : Utils.getInputLines(dag, false).toList()) {
            if (s.isEmpty()) {
                totalBest = Math.max(totalBest, total);
                total = 0;
            } else {
                total += Integer.parseInt(s);
            }
        };

        System.out.println(totalBest);
    }
}
