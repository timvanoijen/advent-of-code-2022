package dag1;

import utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Puzzle2 {

    private static final int dag = 1;

    public static void main(String[] args) {

        int total = 0;
        List<Integer> totals = new ArrayList<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            if (s.isEmpty()) {
                totals.add(total);
                total = 0;
            } else {
                total += Integer.parseInt(s);
            }
        }

        totals.sort(Collections.reverseOrder());
        System.out.println(totals.stream().limit(3).reduce(Integer::sum).orElse(0));
    }
}
